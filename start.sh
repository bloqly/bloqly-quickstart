#!/usr/bin/env bash

if [[ -z "${BLOQLY_HOME}" ]]; then
    echo "BLOQLY_HOME environment variable is not set"
    exit 1
fi

cd ${BLOQLY_HOME}

export APP_PORT=${APP_PORT:-8082}
export ADMIN_PORT=${ADMIN_PORT:-8083}

docker-compose -p bloqly up -d --no-build --no-recreate