#!/usr/bin/env bash

export ADMIN_PORT=${ADMIN_PORT:-8083}

docker exec -it bloqly curl -u admin:admin 127.0.0.1:${ADMIN_PORT}/api/admin/new-key-pair