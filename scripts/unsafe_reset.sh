#!/usr/bin/env bash

if [[ -z "${BLOQLY_HOME}" ]]; then
    echo "BLOQLY_HOME environment variable is not set"
    exit 1
fi

rm -rf ${BLOQLY_HOME}/app/*
rm -rf ${BLOQLY_HOME}/node/*
rm -rf ${BLOQLY_HOME}/db/*

TEMPLATE_DIR=${TEMPLATE_DIR:-${BLOQLY_HOME}/scripts/config_template}

mkdir ${BLOQLY_HOME}/node/config
mkdir ${BLOQLY_HOME}/node/data

cp -r ${TEMPLATE_DIR}/app/* ${BLOQLY_HOME}/app/
cp -r ${TEMPLATE_DIR}/config/* ${BLOQLY_HOME}/node/config/
cp -r ${TEMPLATE_DIR}/data/* ${BLOQLY_HOME}/node/data/