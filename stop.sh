#!/usr/bin/env bash

export APP_PORT=${APP_PORT:-8081}
export ADMIN_PORT=${ADMIN_PORT:-8082}

docker-compose -p bloqly down